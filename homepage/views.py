from django.shortcuts import render,redirect

# Create your views here.
from .models import Jadwal
from .forms import JadwalForm

def home(request):
    return render(request, 'Home.html')

def profile(request):
    return render(request, 'Profile.html')

def experiences(request):
    return render(request, 'Experiences.html')

def skills(request):
    return render(request, 'Skills.html')

def jadwal(request):
    list_jadwal = Jadwal.objects.all()

    context = {
        'list_jadwal':list_jadwal,
    }
    return render(request, 'Jadwal.html',context)

def isijadwal(request):
    form_jadwal = JadwalForm(request.POST or None)

    if request.method == 'POST':
        if form_jadwal.is_valid():
            form_jadwal.save()

        return redirect('homepage:jadwal')

    context = {
        'form_jadwal':form_jadwal,
    }

    return render(request, 'IsiJadwal.html',context)

def contact(request):
    return render(request, 'Contact.html')

def base(request):
    return render(request, 'Base.html')

def delete(request,delete_id):
    Jadwal.objects.filter(id=delete_id).delete()
    return redirect('homepage:jadwal')

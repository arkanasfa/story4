from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('experiences/', views.experiences, name='experiences'),
    path('skills/', views.skills, name='skills'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('isijadwal/', views.isijadwal, name='isijadwal'),
    path('contact/', views.contact, name='contact'),
    path('delete/<int:delete_id>', views.delete, name = 'delete'),
    # dilanjutkan ...
]

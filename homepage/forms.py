from django import forms

from .models import Jadwal

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = {
            'kegiatan',
            'kategori',
            'tempat',
            'tanggal',
            'waktu',
        }


        widgets = {
            'kegiatan' : forms.TextInput(
                attrs={
                    'class' : 'form-control',

                }
            ),

            'kategori' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                }
            ),

            'tempat' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                }
            ),

            'tanggal' : forms.DateInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' :'YYYY-MM-DD'
                }

            ),

            'waktu' : forms.TimeInput(
                attrs={
                    'class' : 'form-control',
                }

            ),
        }

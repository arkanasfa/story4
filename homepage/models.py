from django.db import models
import datetime

# Create your models here.

class Jadwal(models.Model):
    kegiatan = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    tanggal = models.DateField()
    waktu = models.TimeField(default=datetime.time(12,00))

    def __str__(self):
        return "{}.{}".format(self.id,self.kategori)
